# This first lines are used for local testing only
#import pandas as pd
#import os
#routes_df = pd.read_csv(r"C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\routes.csv")
#airports_df = pd.read_csv(r"C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\airports.csv")

"""
This functions plots the distribution of distances between airports for all routes. Remember to import your dataframes before running this 
function. The function has a running time of approximately 10 minutes.
"""


def distance_analysis(routes_df,airports_df):

    """
    The function returns a histogram of the distribution of distances between airports for all routes. the routs_df and airports_df are the
    respective dataframes of the routes and airports, make sure to specify them in the correct order.
    """

    import pandas as pd
    import matplotlib.pyplot as plt
    import numpy as np

    airports_cleaned_df = airports_df[['IATA', 'Latitude', 'Longitude','Name']]
    
    routes_airports_df = routes_df.merge(
        airports_cleaned_df,
        left_on='Source airport',
        right_on='IATA',
        how='left',
        suffixes=('_source', None)
    ).merge(
        airports_cleaned_df,
        left_on='Destination airport',
        right_on='IATA',
        how='left',
        suffixes=('_source', '_destination')
    )

    routes_airports_df = routes_airports_df.iloc[0:1500] # This line is meant to be used for testing porpuses only.

    def distance(source_name, destination_name, routes_airports_df):
        import math


        R = 6371.0

        # Lookup the latitude and longitude for the first airport
        airport1 = routes_airports_df[routes_airports_df['Name_source'] == source_name]
        if airport1.empty:
            return None
        lat1 = airport1.iloc[0]['Latitude_source']
        lon1 = airport1.iloc[0]['Longitude_source']

        # Lookup the latitude and longitude for the second airport
        airport2 = routes_airports_df[routes_airports_df['Name_destination'] == destination_name]
        if airport2.empty:
            return None
        lat2 = airport2.iloc[0]['Latitude_destination']
        lon2 = airport2.iloc[0]['Longitude_destination']

        # Convert latitude and longitude from degrees to radians
        phi1 = math.radians(lat1)
        phi2 = math.radians(lat2)
        delta_phi = math.radians(lat2 - lat1)
        delta_lambda = math.radians(lon2 - lon1)

        # Haversine formula
        a = math.sin(delta_phi / 2)**2 + math.cos(phi1) * math.cos(phi2) * math.sin(delta_lambda / 2)**2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

        # Distance in kilometers
        distance = R * c
        return distance
    
    routes_airports_df['Distance'] = routes_airports_df.apply(lambda row: distance(row['Name_source'], 
                                                                                   row['Name_destination'], routes_airports_df), axis=1)
    routes_airports_df['Distance'].hist(bins=100)
    plt.title('Distribution of distances between airports for all routes')
    plt.xlabel('Distance (km)')
    plt.ylabel('N of )routes')
    return plt.show()

distance_analysis(routes_df, airports_df)