import os
import shutil
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt

class LisOpo:

    
    def __init__(self, source_dir: str, downloads_dir: str = './downloads/'):
        self.source_dir = source_dir
        self.downloads_dir = downloads_dir
        os.makedirs(self.downloads_dir, exist_ok=True)
        
        self.file_names = {
            'routes': 'routes.csv',
            'airplanes': 'airplanes.csv',
            'airlines': 'airlines.csv',
            'airports': 'airports.csv',
        }
        
        for key, filename in self.file_names.items():
            self._copy_and_load(filename)
        
        self._copy_shape_files('ne_110m_admin_0_countries')

    def _copy_and_load(self, filename: str):
        source_path = os.path.join(self.source_dir, filename)
        download_path = os.path.join(self.downloads_dir, filename)
        
        if not os.path.exists(download_path):
            shutil.copy(source_path, download_path)
        
        df = pd.read_csv(download_path)
        setattr(self, filename.split('.')[0], df)

    def _copy_shape_files(self, folder_name: str):
        source_folder = os.path.join(self.source_dir, folder_name)
        download_folder = os.path.join(self.downloads_dir, folder_name)
        
        if not os.path.exists(download_folder):
            shutil.copytree(source_folder, download_folder)

    def plot_airports(self, country):
        # Check if the country exists in the dataset
        if country not in self.airports['Country'].unique():
            return "Error: Country not found in the dataset."

        # Filter the dataset for the specified country
        country_airports = self.airports[self.airports['Country'] == country]

        # Load world map
        world = gpd.read_file(os.path.join(self.downloads_dir, 'ne_110m_admin_0_countries/ne_110m_admin_0_countries.shp'))

        # Make sure the country's map and the airports' points are using the same CRS
        country_map = world[world.NAME == country]
        country_map = country_map.to_crs(epsg=4326)  # WGS84 Lat/Long

        # Plot the world map focused on the country
        fig, ax = plt.subplots()
        country_map.plot(ax=ax, color='white', edgecolor='black')

        # Plot the airports on the map
        gdf = gpd.GeoDataFrame(
            country_airports,
            geometry=gpd.points_from_xy(country_airports.Longitude, country_airports.Latitude)
        )
        gdf = gdf.set_crs(epsg=4326)
        gdf.plot(ax=ax, color='red', marker='o', markersize=5)

        # Set axis limits to the bounds of the country
        minx, miny, maxx, maxy = country_map.total_bounds
        ax.set_xlim(minx, maxx)
        ax.set_ylim(miny, maxy)

        plt.title(f'Airports in {country}')
        plt.show()



     def plot_flight_routes(self, airport_code, internal=False):
        start_airport_row = self.airports[self.airports['IATA'] == airport_code].iloc[0]
        start_country = start_airport_row['Country']
        flights_from_airport = self.routes[self.routes['Source airport'] == airport_code]

        if internal:
            # Finde alle Flughäfen im Startland
            airports_in_country = self.airports[self.airports['Country'] == start_country]['IATA'].unique()
            # Filtere die Flüge, um nur die zu behalten, die innerhalb desselben Landes landen
            internal_flights = flights_from_airport[flights_from_airport['Destination airport'].isin(airports_in_country)]
            flight_counts = internal_flights['Destination airport'].value_counts()
        else:
            # Betrachte alle Flüge vom Startflughafen
            flight_counts = flights_from_airport['Destination airport'].value_counts()

        flight_counts.plot(kind='bar', figsize=(10, 6))
        plt.title(f"Flights from {airport_code}" + (" (Internal Only)" if internal else ""))
        plt.xlabel('Destination Airport')
        plt.ylabel('Number of Flights')
        plt.xticks(rotation=45)
        plt.tight_layout()  
        plt.show()
        

    def plot_most_used_airplane_models(self, countries=None, n=10):
        """
        Visualisiert die N am häufigsten genutzten Flugzeugmodelle basierend auf der Anzahl der Routen.
    
        :param countries: Ein Land oder eine Liste von Ländern. None bedeutet den gesamten Datensatz.
        :param n: Anzahl der darzustellenden Flugzeugmodelle.
        """
        if countries:
            if isinstance(countries, str):
                countries = [countries]  # Einzelnes Land in Liste umwandeln
            airports_in_countries = self.airports[self.airports['Country'].isin(countries)]['IATA'].unique()
            filtered_routes = self.routes[(self.routes['Source airport'].isin(airports_in_countries)) | (self.routes['Destination airport'].isin(airports_in_countries))]
        else:
            filtered_routes = self.routes 
        
        model_counts = filtered_routes['Equipment'].str.split(expand=True).stack().value_counts().head(n)
    
        model_counts.plot(kind='bar', figsize=(10, 6))
        plt.title('Top N Most Used Airplane Models' + (' in ' + ', '.join(countries) if countries else ''))
        plt.xlabel('Airplane Model')
        plt.ylabel('Number of Routes')
        plt.xticks(rotation=45)
        plt.tight_layout()
        plt.show()




# Verwendung der Klasse
source_directory = '/Users/markusgiesbrecht/Documents/Markus/Uni/Business_Analytics/2.Semester/Advanced_Programming/Group assignment/ta/downloads/'
lisopo = LisOpo(source_directory)
lisopo.plot_airports('Germany')  # Beispiel für die Verwendung der Plot-Funktion
lisopo.plot_flight_routes('FRA', internal=True)  # Beispiel für die Verwendung der Plot-Funktion








