import os
import shutil
import pandas as pd  # Imports are usually placed at the top, grouped by standard, third-party, and local modules

routes_path = r'C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\routes.csv'
airplanes_path = r'C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\airplanes.csv'
airlines_path = r'C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\airports.csv'  # This seems to be a typo; it should probably be 'airlines.csv'
airport_path = r'C:\Users\dador\Desktop\Davide\NOVASBE\ADPRO\Group Project\airports.csv'

filename_routes = 'routes.csv'
filename_airplanes = 'airplanes.csv'
filename_airlines = 'airlines.csv'
filename_airport = 'airport.csv'


class LisOpo:
    """
    Initializes the class by copying specified CSV files into a 'downloads/' directory and loading them into pandas DataFrames.
    """
    
    def __init__(self, routes_path, filename_routes, airplanes_path, filename_airplanes, airlines_path, filename_airlines, airport_path, filename_airport):
        self.routes_path = routes_path
        self.filename_routes = filename_routes
        
        self.airplanes_path = airplanes_path
        self.filename_airplanes = filename_airplanes
        
        self.airlines_path = airlines_path
        self.filename_airlines = filename_airlines

        self.airport_path = airport_path
        self.filename_airport = filename_airport
        
        self.downloads_path = 'downloads/'
        
        self.routes_file_path = os.path.join(self.downloads_path, filename_routes)
        self.airplanes_file_path = os.path.join(self.downloads_path, filename_airplanes)
        self.airlines_file_path = os.path.join(self.downloads_path, filename_airlines)
        self.airport_file_path = os.path.join(self.downloads_path, filename_airport)
        

    
        """Ensures the downloads directory exists."""
        os.makedirs(self.downloads_path, exist_ok=True)

    
        """Copies data files to the downloads directory if they don't already exist."""
        for path, file_path, filename in [(self.routes_path, self.routes_file_path, self.filename_routes),
                                          (self.airplanes_path, self.airplanes_file_path, self.filename_airplanes),
                                          (self.airlines_path, self.airlines_file_path, self.filename_airlines),
                                          (self.airport_path, self.airport_file_path, self.filename_airport)]:
            if not os.path.exists(file_path):
                print(f"Copying {filename} to {file_path}...")
                shutil.copy(path, file_path)
                print(f"Copied {filename} successfully.")
            else:
                print(f"{filename} already exists at {file_path}. No copying needed.")

    
        """Reads the datasets into pandas DataFrames."""
        self.df_routes = pd.read_csv(self.routes_file_path)
        self.df_airplanes = pd.read_csv(self.airplanes_file_path)
        self.df_airlines = pd.read_csv(self.airlines_file_path)
        self.df_airport = pd.read_csv(self.airport_file_path)
        # Example of printing the first few rows of each dataframe
        #print('AIRPLANES DATAFRAME \n', self.df_airplanes.head())
        #print('ROUTES DATAFRAME \n', self.df_routes.head())
        #print('AIRLINES DATAFRAME \n', self.df_airlines.head())
        #print('AIRPORT DATAFRAME \n', self.df_airport.head())

    def distances(self, airport1, airport2):
        import real_distances
        return real_distances.real_distances(airport1, airport2, self.df_airport)
                
analysis = LisOpo(routes_path, filename_routes, airplanes_path, filename_airplanes, airlines_path, filename_airlines, airport_path, filename_airport)

distance = analysis.distances('Narsarsuaq Airport', 'Baker Lake Airport')
print(distance)
