import geopandas as gpd
import pandas as pd
import matplotlib.pyplot as plt

def plot_airports(country):
    # Load the dataset
    airports = pd.read_csv('downloads/airport.csv')

    # Check if the country exists in the dataset
    if country not in airports['Country'].unique():
        return "Error: Country not found in the dataset."

    # Filter the dataset for the specified country
    country_airports = airports[airports['Country'] == country]

    # Load world map
    world = gpd.read_file('downloads/ne_110m_admin_0_countries/ne_110m_admin_0_countries.shp')

    # Make sure the country's map and the airports' points are using the same CRS
    country_map = world[world.NAME == country]
    country_map = country_map.to_crs(epsg=4326)  # WGS84 Lat/Long

    # Plot the world map focused on the country
    fig, ax = plt.subplots()
    country_map.plot(ax=ax, color='white', edgecolor='black')

    # Plot the airports on the map
    gdf = gpd.GeoDataFrame(
        country_airports,
        geometry=gpd.points_from_xy(country_airports.Longitude, country_airports.Latitude)
    )
    gdf = gdf.set_crs(epsg=4326)  # Ensure the airports' CRS matches the world map's CRS
    gdf.plot(ax=ax, color='red', marker='o', markersize=5)

    # Set axis limits to the bounds of the country
    minx, miny, maxx, maxy = country_map.total_bounds
    ax.set_xlim(minx, maxx)
    ax.set_ylim(miny, maxy)

    plt.title(f'Airports in {country}')
    plt.show()

plot_airports('Denmark')
plot_airports('